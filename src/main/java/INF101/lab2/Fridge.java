package INF101.lab2;
import java.util.ArrayList;
import java.util.NoSuchElementException;


public class Fridge implements IFridge {
    ArrayList<FridgeItem> fridge;


    public Fridge(){
        fridge = new ArrayList<FridgeItem>();
    }

    public int nItemsInFridge() {
        int size = fridge.size();
        return size;
    }

    
    public int totalSize(){
        int maxsize = 20;
        return maxsize;
    }

    public boolean placeIn(FridgeItem item){
        if (nItemsInFridge() < totalSize()) {
            fridge.add(item);
            return true; 

        } else {
            return false;
        }
    }


    public void takeOut(FridgeItem item) {
        if (fridge.contains(item)) {
            fridge.remove(item);
        } else {
            throw new NoSuchElementException("This item not in fridge");
        }

    }

    public void emptyFridge() {
        fridge.clear();
    }


    public ArrayList<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = findExpiredItems();
        fridge.removeAll(expiredItems);
        return expiredItems;
    }

    private ArrayList<FridgeItem> findExpiredItems(){
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for(FridgeItem item : fridge) {
            if (item.hasExpired()){
                expiredItems.add(item);
            }
        
        }
        return expiredItems;
    }
}
